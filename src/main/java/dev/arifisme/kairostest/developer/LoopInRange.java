package dev.arifisme.kairostest.developer;


import java.util.Scanner;

public class LoopInRange {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        int givenRange = scanner.nextInt();

        System.out.println(SkillDeveloper.loopInGivenRange(givenRange));
    }
}
