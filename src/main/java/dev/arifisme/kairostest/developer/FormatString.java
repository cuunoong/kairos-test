package dev.arifisme.kairostest.developer;


import java.util.Scanner;

public class FormatString {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        String givenString = scanner.nextLine();

        System.out.println(SkillDeveloper.formatGivenString(givenString));
    }
}
