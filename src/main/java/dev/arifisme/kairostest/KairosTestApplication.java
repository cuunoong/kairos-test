package dev.arifisme.kairostest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KairosTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(KairosTestApplication.class, args);
	}

}
