package dev.arifisme.kairostest.product;

import dev.arifisme.kairostest.utils.Formatter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;
    private ProductService underTest;

    @BeforeEach
    void setUp() {
        underTest = new ProductService(productRepository, new ProductDTOMapper());
    }

    @Test
    void canGetAllProducts() {
        // when
        underTest.getAllProducts();

        // then
        verify(productRepository).findAll();
    }

    @Test
    void canSearchProducts() {
        // Given
        String query = "b";

        // when
        underTest.searchProducts(query);

        // then
        verify(productRepository).searchProducts(query);
    }

    @Test
    void canAddProduct() {
        // given
        ProductRequest productRequest = new ProductRequest(
                "Pen",
                "P001",
                10,
                "2013-10-12");

        // when
        underTest.addProduct(productRequest);

        //then
        ArgumentCaptor<Product> productArgumentCaptor = ArgumentCaptor.forClass(Product.class);

        verify(productRepository).save(productArgumentCaptor.capture());

        Product expected = productArgumentCaptor.getValue();

        assertThat(expected.getName()).isEqualTo(productRequest.name());
        assertThat(expected.getCode()).isEqualTo(productRequest.code());
        assertThat(expected.getTotal()).isEqualTo(productRequest.total());
        assertThat(expected.getDate()).isEqualTo(Formatter.toDateTime("2013-10-12"));
    }

    @Test
    void addProductWillThrowWhenCodeIsTaken() {
        // given
        ProductRequest productRequest = new ProductRequest(
                "Pen",
                "P001",
                10,
                "2013-10-12");

        given(productRepository.selectExistCode(anyString()))
                .willReturn(true);

        // expect
        assertThatThrownBy(() -> underTest.addProduct(productRequest))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("400 BAD_REQUEST \"Code " + productRequest.code() + " taken\"");
    }

    @Test
    void canGetProductById() {
        // given
        Integer productId = 1;

        given(productRepository.findById(anyInt()))
                .willReturn(Optional.of(Product.builder().build()));

        // expect
        ProductDTO productDTO = underTest.getProductById(productId);
        assertThat(productDTO).isInstanceOf(ProductDTO.class);
    }

    @Test
    void getProductByCodeWillThrowWhenCodeIsNotFound() {
        // given
        Integer productId = 1;

        given(productRepository.findById(anyInt()))
                .willReturn(Optional.empty());

        // expect
        assertThatThrownBy(() -> underTest.getProductById(productId))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("404 NOT_FOUND \"Product with id " + productId + " not found\"");
    }

    @Test
    void canUpdateProduct() {
        // given
        Integer productId = 1;
        ProductRequest productRequest = new ProductRequest(
                "Pen",
                "P001",
                10,
                "2013-10-12");

        Product product = Product.builder().build();

        given(productRepository.findById(anyInt()))
                .willReturn(Optional.of(product));
        given(productRepository.selectExistCode(anyString()))
                .willReturn(false);
        // when
        underTest.updateProduct(productId, productRequest);

        // then
        verify(productRepository).save(product);
    }

    @Test
    void canUpdateProductWillThrowWhenProductIdNotFound() {
        // given
        Integer productId = 1;
        ProductRequest productRequest = new ProductRequest(
                "Pen",
                "P001",
                10,
                "2013-10-12");

        given(productRepository.findById(anyInt()))
                .willReturn(Optional.empty());

        // then
        assertThatThrownBy(() -> underTest.updateProduct(productId, productRequest))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("404 NOT_FOUND \"Product with id " + productId + " not found\"");
    }

    @Test
    void canUpdateProductWillThrowWhenCodeHasExist() {
        // given
        Integer productId = 1;
        ProductRequest productRequest = new ProductRequest(
                "Pen",
                "P001",
                10,
                "2013-10-12");
        Product product = Product.builder().build();

        given(productRepository.findById(anyInt()))
                .willReturn(Optional.of(product));
        given(productRepository.selectExistCode(anyString()))
                .willReturn(true);

        // then
        assertThatThrownBy(() -> underTest.updateProduct(productId, productRequest))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("400 BAD_REQUEST \"Code " + productRequest.code() + " taken\"");
    }

    @Test
    void canDeleteProduct() {
        Integer productId = 1;

        underTest.deleteProduct(productId);

        verify(productRepository).deleteById(productId);
    }
}