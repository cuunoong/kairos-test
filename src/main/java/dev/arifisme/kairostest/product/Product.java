package dev.arifisme.kairostest.product;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "nama_barang", length = 200, nullable = false)
    private String name;
    @Column(name = "kode_barang", length = 50, nullable = false)
    private String code;
    @Column(name = "jumlah_barang", length = 10, nullable = false)
    private Integer total;
    @Column(name = "tanggal", nullable = false)
    private LocalDateTime date;
}
