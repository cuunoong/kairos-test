package dev.arifisme.kairostest.product;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
@DataJpaTest
class ProductRepositoryTest {

    @Autowired
    private ProductRepository underTest;


    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void itShouldSelectWhenProductCodeExist() {
        // Given
        String code = "B0021";
        Product product = Product.builder()
                .name("Book")
                .code(code)
                .total(10)
                .date(LocalDateTime.now())
                .build();
        underTest.save(product);

        //When
        Boolean expected = underTest.selectExistCode(code);

        // Expect
        assertThat(expected).isTrue();
    }

    @Test
    void itShouldSelectWhenProductCodeDoesNotExist() {
        // Given
        String code = "B0021";

        //When
        Boolean expected = underTest.selectExistCode(code);

        // Expect
        assertThat(expected).isFalse();
    }

    @Test
    void itShouldFindProductByNameContainsGivenName() {
        // Given
        Product product = Product.builder()
                .name("Book")
                .code("B0021")
                .total(10)
                .date(LocalDateTime.now())
                .build();
        underTest.save(product);

        // When
        List<Product> products = underTest.findByNameContains("oo");
        int expected = products.size();

        // Expected
        assertThat(expected).isEqualTo(1);
    }

    @Test
    void itShouldFindProductByCodeContainsGivenCode() {

        // Given
        Product product = Product.builder()
                .name("Book")
                .code("B0021")
                .total(10)
                .date(LocalDateTime.now())
                .build();
        underTest.save(product);

        // When
        List<Product> products = underTest.findByCodeContains("21");
        int expected = products.size();

        // Expected
        assertThat(expected).isEqualTo(1);
    }

    @Test
    void itShouldFindProductByGivenCode() {

        // Given
        String code = "B0021";
        Product product = Product.builder()
                .name("Book")
                .code(code)
                .total(10)
                .date(LocalDateTime.now())
                .build();
        underTest.save(product);

        // When
        Optional<Product> optionalProduct = underTest.findByCode(code);
        Boolean expected = optionalProduct.isPresent();

        // Expected
        assertThat(expected).isTrue();
    }

    @Test
    void itShouldSearchProductByQueryName() {
        // Given
        String query = "K";
        Product product = Product.builder()
                .name("Book")
                .code("B0001")
                .total(10)
                .date(LocalDateTime.now())
                .build();
        underTest.save(product);

        // When
        List<Product> products = underTest.searchProducts(query);
        Boolean expected = !products.isEmpty();

        // Expected
        assertThat(expected).isTrue();
    }

    @Test
    void itShouldSearchProductByQueryCode() {
        // Given
        String query = "0";
        Product product = Product.builder()
                .name("Book")
                .code("B0001")
                .total(10)
                .date(LocalDateTime.now())
                .build();
        underTest.save(product);

        // When
        List<Product> products = underTest.searchProducts(query);
        Boolean expected = !products.isEmpty();

        // Expected
        assertThat(expected).isTrue();
    }
}