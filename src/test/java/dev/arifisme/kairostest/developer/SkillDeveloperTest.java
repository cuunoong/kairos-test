package dev.arifisme.kairostest.developer;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SkillDeveloperTest {

    @Test
    void itShouldShowAllFractions() {
        // Given
        int number = 1225441;

        // When
        String result = SkillDeveloper.numberToFractions(number);

        // Expect
        String expected =
                """
                        1000000
                        200000
                        20000
                        5000
                        400
                        40
                        1""";

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void itShouldShowAllFractionsToo() {
        // Given
        int number = 103012332;

        // When
        String result = SkillDeveloper.numberToFractions(number);

        // Expect
        String expected =
                """
                        100000000
                        3000000
                        10000
                        2000
                        300
                        30
                        2""";

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void itShouldShowAllCharCountOnGivenString() {
        // Given
        String string = "hello world";

        // When
        String result = SkillDeveloper.countCharOnGivenString(string);

        // Expected
        String expected =
                """
                        h – 1
                        e – 1
                        l – 3
                        o – 2
                        w – 1
                        r – 1
                        d – 1""";

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void itShouldShowAllCharCountOnGivenStringToo() {
        // Given
        String string = "Arif Iskandar";

        // When
        String result = SkillDeveloper.countCharOnGivenString(string);

        // Expected
        String expected =
                """
                        A – 1
                        r – 2
                        i – 1
                        f – 1
                        I – 1
                        s – 1
                        k – 1
                        a – 2
                        n – 1
                        d – 1""";

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void itShouldShowNumberInRageAndReplaceSomeNumber() {
        // Given
        int range = 19;

        // When
        String result = SkillDeveloper.loopInGivenRange(range);

        // Expect
        String expected = "1 2 3 4 5 6 7 8 9 IDIC 11 LPS 13 14 IDIC 16 17 LPS 19";

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void itShouldConvertGivenTextToTitleAndRegularFormat() {
        // Given
        String range = "SeLamAT PAGi semua halOo";

        // When
        String result = SkillDeveloper.formatGivenString(range);

        // Expect
        String expected = "Format Judul: Selamat Pagi Semua Haloo\n" +
                "Format Biasa: Selamat pagi semua haloo";

        assertThat(result).isEqualTo(expected);
    }
}