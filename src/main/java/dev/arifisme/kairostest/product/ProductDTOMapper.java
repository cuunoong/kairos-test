package dev.arifisme.kairostest.product;

import dev.arifisme.kairostest.utils.Formatter;
import org.springframework.stereotype.Service;
import java.util.function.Function;

@Service
public class ProductDTOMapper implements Function<Product, ProductDTO> {
    @Override
    public ProductDTO apply(Product product) {
        return new ProductDTO(
                product.getId(),
                product.getName(),
                product.getCode(),
                product.getTotal(),
                Formatter.toDateTimeString(product.getDate()),
                Formatter.toDateTimeString(product.getDate(), "yyyy-MM-dd")
        );
    }
}
