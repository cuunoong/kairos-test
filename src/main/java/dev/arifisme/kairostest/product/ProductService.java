package dev.arifisme.kairostest.product;

import dev.arifisme.kairostest.utils.Formatter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor @Slf4j
public class ProductService {
    private ProductRepository productRepository;
    private ProductDTOMapper productDTOMapper;

    public List<ProductDTO> getAllProducts() {
        return  productRepository.findAll()
                .stream()
                .map(productDTOMapper)
                .toList();
    }

    public List<ProductDTO> searchProducts(String query) {
        return productRepository.searchProducts(query)
                .stream()
                .map(productDTOMapper)
                .toList();
    }

    public void addProduct(ProductRequest productRequest) {
        Boolean isCodeExist = productRepository.selectExistCode(productRequest.code());

        if(isCodeExist) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Code " + productRequest.code() + " taken"
            );
        }

        Product product = Product
                .builder()
                .name(productRequest.name())
                .code(productRequest.code())
                .total(productRequest.total())
                .date(Formatter.toDateTime(productRequest.date()))
                .build();

        productRepository.save(product);
    }

    public ProductDTO getProductById(Integer productId) {
        Optional<Product> product = productRepository.findById(productId);
        if(!product.isPresent()) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "Product with id " + productId + " not found"
            );
        }
        return productDTOMapper.apply(product.get());
    }

    public void updateProduct(Integer productId, ProductRequest productRequest) {
        Optional<Product> productOptional = productRepository.findById(productId);
        if(!productOptional.isPresent()) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "Product with id " +  productId + " not found"
            );
        }

        Product product = productOptional.get();

        if(!productRequest.code().equals(product.getCode())) {
            Boolean isCodeExist = productRepository.selectExistCode(productRequest.code());

            if(isCodeExist) {
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "Code " + productRequest.code() + " taken"
                );
            }
        }

        product.setCode(productRequest.code());
        product.setName(productRequest.name());
        product.setTotal(productRequest.total());
        product.setDate(Formatter.toDateTime(productRequest.date()));

        productRepository.save(product);
    }

    void deleteProduct(Integer productId) {
        productRepository.deleteById(productId);
    }
}
