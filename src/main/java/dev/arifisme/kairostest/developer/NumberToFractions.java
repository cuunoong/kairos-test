package dev.arifisme.kairostest.developer;


import java.util.Scanner;

public class NumberToFractions {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        int givenNumber = scanner.nextInt();

        System.out.println(SkillDeveloper.numberToFractions(givenNumber));
    }
}
