package dev.arifisme.kairostest.product;

public record ProductRequest (
        String name,
        String code,
        Integer total,
        String date
){
}
