package dev.arifisme.kairostest;

import dev.arifisme.kairostest.product.Product;
import dev.arifisme.kairostest.product.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DatabaseSeeder implements CommandLineRunner {
    private final ProductRepository productRepository;

    @Override
    public void run(String... args) {
        productRepository.saveAll(List.of(
           Product
                   .builder()
                   .name("Kelas Belajar Programming")
                   .code("KBP01")
                   .total(1000)
                   .date(LocalDateTime.now())
                   .build()
        ));
    }
}
