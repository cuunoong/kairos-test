package dev.arifisme.kairostest.product;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/product")
@Slf4j
public class ProductController {

    private ProductService productService;

    @GetMapping
    String getAllProducts(@RequestParam(required = false) String query, Model model){
        List<ProductDTO> products;
        if(query == null || query.isEmpty())
            products = productService.getAllProducts();
        else {
            products = productService.searchProducts(query);
            model.addAttribute("query", query);
        }
        model.addAttribute("products", products);
        return "index";
    }

    @GetMapping("/create")
    String create() {
        return "create";
    }

    @PostMapping()
    String store(@ModelAttribute ProductRequest productRequest, RedirectAttributes redirectAttributes) {
        try {
            productService.addProduct(productRequest);
            redirectAttributes.addFlashAttribute(
                    "message",
                    "Produk " + productRequest.name() + " berhasil ditambah!");
        } catch (ResponseStatusException exception) {
            redirectAttributes.addFlashAttribute(
                    "message",
                    exception.getMessage());
            redirectAttributes.addFlashAttribute("error", true);
        }
        return "redirect:/product";
    }

    @GetMapping("{productId}")
    public String show(@PathVariable("productId") Integer productId,RedirectAttributes redirectAttributes, Model model) {

        try {
            ProductDTO productDTO = productService.getProductById(productId);

            model.addAttribute("product", productDTO);
            return "show";
        } catch (ResponseStatusException exception) {
            redirectAttributes.addFlashAttribute(
                    "message",
                    exception.getMessage());
            redirectAttributes.addFlashAttribute("error", true);
            return "redirect:/product";
        }
    }

    @PostMapping("{productId}")
    public String save(@PathVariable("productId") Integer productId, @ModelAttribute ProductRequest productRequest, RedirectAttributes redirectAttributes) {

        try {
            productService.updateProduct(productId, productRequest);
            redirectAttributes.addFlashAttribute(
                    "message",
                    "Product berhasil disimpan");
        } catch (ResponseStatusException exception) {
            redirectAttributes.addFlashAttribute(
                    "message",
                    exception.getMessage());
            redirectAttributes.addFlashAttribute("error", true);
        }
        return "redirect:/product";
    }

    @PostMapping("{productId}/delete")
    public String deleteProduct(@PathVariable("productId") Integer productId, RedirectAttributes redirectAttributes) {

        try {
            productService.deleteProduct(productId);
            redirectAttributes.addFlashAttribute(
                    "message",
                    "Product berhasil dihapus");
        } catch (ResponseStatusException exception) {
            redirectAttributes.addFlashAttribute(
                    "message",
                    exception.getMessage());
            redirectAttributes.addFlashAttribute("error", true);
        }
        return "redirect:/product";
    }
}
