package dev.arifisme.kairostest.product;

public record ProductDTO (
        Integer id,
        String name,
        String code,
        Integer total,
        String date,
        String inputDate
){
}
