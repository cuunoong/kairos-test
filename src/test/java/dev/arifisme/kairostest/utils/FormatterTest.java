package dev.arifisme.kairostest.utils;

import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
class FormatterTest {

    @Test
    void itShouldReturnAFormatDateString() {
        // given
        LocalDateTime dateTime = LocalDateTime.of(2023, 12, 21, 0,0);

        // when
        String expected = Formatter.toDateTimeString(dateTime);

        // expect
        String formatted = "21 December 2023";
        assertThat(expected).isEqualTo(formatted);
    }

    @Test
    void itShouldConvertStringToGivenLocalDateTime() {
        // given
        String date = "2023-12-21";

        // when
        LocalDateTime expected = Formatter.toDateTime(date);

        // expect
        LocalDateTime dateTime = LocalDateTime.of(2023, 12, 21, 0,0);
        assertThat(expected).isEqualTo(dateTime);
    }
}