package dev.arifisme.kairostest.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>, PagingAndSortingRepository<Product, Integer> {

    @Query("" +
            "SELECT CASE WHEN " +
            "COUNT (p) > 0 THEN TRUE ELSE FALSE END " +
            "FROM Product p " +
            "WHERE p.code = :code ")
    Boolean selectExistCode(@Param("code") String code);

    Optional<Product> findByCode(String code);

    List<Product> findByNameContains(String name);

    List<Product> findByCodeContains(String code);

    @Query("" +
            "SELECT p FROM Product p " +
            "WHERE lower(p.name) LIKE lower(concat('%', :query, '%')) OR " +
            "lower(p.code) LIKE lower(concat('%', :query, '%'))")
    List<Product> searchProducts(@Param("query") String query);

}
