package dev.arifisme.kairostest.developer;


import java.util.ArrayList;
import java.util.List;

public class SkillDeveloper {
    static String numberToFractions(int numberInput) {
        int zeroCount = 1;
        int tempNumber = numberInput / 10;

        String result = String.format("%d", numberInput % 10);

        while (tempNumber > 0) {

            // calculate fractions
            int fraction = (int) ((tempNumber % 10) * Math.pow(10, zeroCount));
            if(fraction > 0)
                result = String.format("%d\n%s", fraction, result);

            tempNumber = tempNumber / 10;
            zeroCount++;
        }

        return result;
    }

    static String countCharOnGivenString(String string) {
        List<String> availableCharacters = new ArrayList<>();
        List<Integer> charCount = new ArrayList<>();

        String[] allCharacters = string.split("");

        for (String character:
                allCharacters) {
            if(character.equals(" "))
                continue;
            if(!availableCharacters.contains(character))
            {
                availableCharacters.add(character);
                charCount.add(1);
            } else {
                int index = availableCharacters.indexOf(character);
                charCount.set(index, charCount.get(index) + 1);
            }
        }

        String result = "";
        for (int i = 0; i < availableCharacters.size(); i++) {
            if(i == 0)
                result = String.format("%s – %d", availableCharacters.get(i), charCount.get(i));
            else
                result = String.format("%s\n%s – %d", result,  availableCharacters.get(i), charCount.get(i));
        }
        return result;
    }

    static String loopInGivenRange(int range) {
        String result = "";

        for (int i = 1; i <= range; i++) {
            if(i % 5 == 0 && i != 5)
                result = String.format("%s %s", result, "IDIC");
            else if(i % 6 == 0 && i != 6)
                result = String.format("%s %s", result, "LPS");
            else if(i == 1)
                result = "1";
            else result = String.format("%s %d", result, i);
        }
        return result;
    }

    static String formatGivenString(String string){
        String[] allWords = string.split(" ");

        String titleFormat = "";
        String regularFormat = "";

        for (String word:
             allWords) {

            if(word.length() == 1) {
                titleFormat = String.format("%s %s", titleFormat, word.toUpperCase());
                regularFormat = String.format("%s %s", regularFormat, word.toLowerCase());
            } else {
                titleFormat = String.format("%s %s%s", titleFormat, word.substring(0,1).toUpperCase(), word.substring(1).toLowerCase());
                regularFormat = String.format("%s %s", regularFormat, word.toLowerCase());
            }
        }

        regularFormat = String.format("%s%s", regularFormat.substring(0,2).toUpperCase(), regularFormat.substring(2));

        return String.format(
                "Format Judul:%s\n" +
                "Format Biasa:%s", titleFormat, regularFormat);
    }
}
