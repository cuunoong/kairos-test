package dev.arifisme.kairostest.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Formatter {
    public static String toDateTimeString(LocalDateTime dateTime) {
        return  Formatter.toDateTimeString(dateTime,"dd MMMM yyyy" );
    }
    public  static String toDateTimeString(LocalDateTime dateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        if(dateTime == null)
            return LocalDateTime.now().format(formatter);
        return  dateTime.format(formatter);
    }



    public static LocalDateTime toDateTime(String date){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, dateTimeFormatter);
        return localDate.atStartOfDay();
    }
}
