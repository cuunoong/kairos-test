package dev.arifisme.kairostest.developer;


import java.util.Scanner;

public class CountAllCharacters {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        String givenString = scanner.nextLine();

        System.out.println(SkillDeveloper.countCharOnGivenString(givenString));
    }
}
